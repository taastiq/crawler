import json
import os
from collections import defaultdict

import boto3
from boto3.dynamodb.conditions import Key
from botocore.exceptions import ClientError

DYNAMODB_TABLE_NAME = os.environ.get('SITES_TABLE_NAME')


class DynamoDBClient(object):
    dynamodb = boto3.resource('dynamodb')
    dynamodb_client = boto3.client('dynamodb')
    table = dynamodb.Table(DYNAMODB_TABLE_NAME)
    
    @staticmethod
    def get(pk):
        response = DynamoDBClient.dynamodb_client.query(
            TableName = DYNAMODB_TABLE_NAME,
            KeyConditionExpression='PK=:PK',
            ExpressionAttributeValues = {
                ':PK': {'S': '{}'.format(pk)}
            },
            ProjectionExpression = "PK, SK, fullURL, latestTestResults"
        )
        # return response
        if response['Count'] > 0:
            return response['Items']
        # else:
        #     return {}

    @staticmethod
    def add_test_result(tr):
        page_pk = tr.get('PK')
        page_sk = tr.get('SK')
        page_id = tr.get('id')
        page_parent_id = tr.get('parentID')
        timestamp = tr.get('timestamp')
        test_type = tr.get('testType')
        resolution = tr.get('resolution')
        screenshotID = tr.get('screenshotID')
        has_failed = tr.get('hasFailed')
       
        
        result = DynamoDBClient.table.update_item(  
            Key={
                'PK': '{}'.format(page_pk),
                'SK': '{}'.format(page_sk)
            },
            UpdateExpression="SET latestTestResults.#key = :value",
            ExpressionAttributeNames = {
                "#key": resolution
            },
            ExpressionAttributeValues = {
                ":value": {
                    'hasFailed': has_failed, 
                    'screenshotID': screenshotID,
                    'timestamp': timestamp
                }
            }
            
        )

        return result


def process_test_completion(pk):
    site_items = DynamoDBClient.get(pk)
    urls_with_problems = defaultdict(dict)
    urls_without_problems = set()
    for item in site_items:
        fullURL = item.get('fullURL', {}).get('S')
        problems_exist = False
        latestTestResults = item.get('latestTestResults', {}).get('M')
        for deviceType, data_map in latestTestResults.items():
            data = data_map.get('M')
            hasFailed = data.get('hasFailed', {}).get('BOOL')
            if hasFailed:
                problems_exist = True
                screenshotID = data.get('screenshotID', {}).get('S')
                urls_with_problems[fullURL][deviceType] = screenshotID
        if not problems_exist:
            urls_without_problems.add(fullURL)
            
    return urls_with_problems, urls_without_problems

def send_email(site, test_results_fail, test_results_success):

    host = site.replace('SITE|', '')

    SENDER = "awarebee <taastiq@gmail.com>"
    RECIPIENT = "kdyachkov@gmail.com"
    CONFIGURATION_SET = "Dev"
    AWS_REGION = "us-east-2"
    SUBJECT = "awarebee test results"
    
    # The email body for recipients with non-HTML email clients.
    BODY_TEXT = ("Change Me")
    
    failed_results_html = ""
    for url, screenshots_by_device in test_results_fail.items():
        failed_results_html += "<li>{}".format(url)
        failed_results_html += "<ul>"
        for deviceType, screenshotID in screenshots_by_device.items():
            failed_results_html += '<li><a href="{url}">{device}</a></li>'.format(url="https://taas-crawler.s3.us-east-2.amazonaws.com/"+screenshotID, device=deviceType)
        failed_results_html += "</ul>"
        failed_results_html += "</li>"
    
    success_results_html = ""
    for url in test_results_success:
        success_results_html += "<li>{}</li>".format(url)
    
    # The HTML body of the email.
    BODY_HTML = """<html>
    <head></head>
    <body>
      <h1>awarebee test results for {host}</h1>
      <h3>The following pages contain unexpected differences</h3>
      <ul>
        {failed_results_html}
      </ul>
      <h3>The following pages are the same as last time</h3>
      <ul>
        {success_results_html}
      </ul>
    </body>
    </html>
    """.format(host=host, failed_results_html=failed_results_html, success_results_html=success_results_html)            
    
    print(BODY_HTML)
    # return
    # The character encoding for the email.
    CHARSET = "UTF-8"
    
    # Create a new SES resource and specify a region.
    client = boto3.client('ses',region_name=AWS_REGION)
    
    # Try to send the email.
    try:
        #Provide the contents of the email.
        response = client.send_email(
            Destination={
                'ToAddresses': [
                    RECIPIENT,
                ],
            },
            Message={
                'Body': {
                    'Html': {
                        'Charset': CHARSET,
                        'Data': BODY_HTML,
                    },
                    'Text': {
                        'Charset': CHARSET,
                        'Data': BODY_TEXT,
                    },
                },
                'Subject': {
                    'Charset': CHARSET,
                    'Data': SUBJECT,
                },
            },
            Source=SENDER,
            ConfigurationSetName=CONFIGURATION_SET,
        )
    # Display an error if something goes wrong.	
    except ClientError as e:
        print(e.response['Error']['Message'])
    else:
        print("Email sent! Message ID:"),
        print(response['MessageId'])


def lambda_handler(event, context):
    print(event)
    records = event['Records']
    for record in records:
        print(record)
        msg_json = record["body"]
        print(msg_json)
        msg = json.loads(msg_json)
        print(msg)
        pk = msg.get('PK')
        sk = msg.get('SK')
        # print(pk)
        
        if msg.get('visualTestCompleted'):
            print("Processing Test Results for {}".format(pk))
            test_results_fail, test_results_success = process_test_completion(pk)
            send_email(pk, test_results_fail, test_results_success)
        else:
            print("Adding test results for {}-{}".format(pk, sk))
            DynamoDBClient.add_test_result(msg)
