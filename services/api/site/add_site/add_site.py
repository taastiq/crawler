from typing import Tuple, Dict
import os
import boto3
from boto3.dynamodb.conditions import Key
import json
import decimal

DYNAMODB_TABLE_NAME = os.environ.get('SITES_TABLE_NAME')
SQS_QUEUE_NAME = os.environ.get('CRAWLER_QUEUE_NAME')

STATUS_OK = 200
STATUS_CREATED = 201
STATUS_CONFLICT = 409
STATUS_ERROR = 500

# Helper class to convert a DynamoDB item to JSON.
class DecimalEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            if o % 1 > 0:
                return float(o)
            else:
                return int(o)
        return super(DecimalEncoder, self).default(o)


class DynamoDBClient(object):
    dynamodb = boto3.resource('dynamodb')
    dynamodb_client = boto3.client('dynamodb')
    table = dynamodb.Table(DYNAMODB_TABLE_NAME)
    
    @staticmethod
    def get_site(domain: str, just_count: bool=False):
        kwargs = {
            'TableName': DYNAMODB_TABLE_NAME,
            'KeyConditionExpression': 'PK=:PK AND begins_with(SK, :SK)',
            'ExpressionAttributeValues': {
                ':PK': {'S': 'SITE|{}'.format(domain)},
                ':SK': {'S': 'PAGE|'}
            }
        }
        if just_count:
            kwargs['Select'] = 'COUNT'
        response = DynamoDBClient.dynamodb_client.query(**kwargs)
        return response

    @staticmethod
    def put(hostname: str) -> dict:
        item = {
            'PK': {'S': 'SITE|{}'.format(hostname)},
            'SK': {'S': 'META|{}'.format(hostname)},
            'hostname': {'S': hostname},
            'isCrawled': {'BOOL': False},
            'isCrawlInProgress': {'BOOL': False},
            'numPagesTotal': {'N': 0},
            'numPagesCrawled': {'N': 0},
            'forceReCrawl': {'BOOL': False},
        }
        
        DynamoDBClient.dynamodb_client.put_item(
            TableName = DYNAMODB_TABLE_NAME,
            Item = item
        )
        return item
        

class SQSClient(object):
    sqs = boto3.resource('sqs')
    queue_client = sqs.get_queue_by_name(QueueName=SQS_QUEUE_NAME)
    @staticmethod
    def add_to_queue(url: str) -> dict:
        response = SQSClient.queue_client.send_message(MessageBody=url, MessageAttributes={
            'URL': {
                'StringValue': url,
                'DataType': 'String'
            }
        })

        print(response.get('MessageId'))
        print(response.get('MD5OfMessageBody'))
        return response


def add_site(site: str) -> Tuple[dict, int]:
    count = DynamoDBClient.get_site(site).get('Count')
    msg = ''
    code = STATUS_ERROR 
    
    if count > 0:
        msg = '{} could not be added because it already exists'.format(site)
        code = STATUS_CONFLICT
    else:
        msg = '{} added to the queue to be crawled'.format(site)
        code = STATUS_CREATED
        SQSClient.add_to_queue(site)
    
    return {'message': msg}, code


def lambda_handler(event, context) -> dict:
    method = event['requestContext']['http']['method']
    payload = event['queryStringParameters'] if method == 'GET' else json.loads(event['body'])

    data = {}
    status_code = STATUS_ERROR

    if method == 'POST':
        site = payload.get('site')
        data, status_code = add_site(site)

    status = 'success' if 200 <= status_code < 300 else 'error'
    resp = {'status': status, 'data': data}
    resp_json = json.dumps(resp, cls=DecimalEncoder)

    return {'body': resp_json, 'statusCode': status_code, "headers": {"Content-Type": "application/json"}}
