from typing import Tuple, Dict
import os
import boto3
from boto3.dynamodb.conditions import Key
import json
import decimal

DYNAMODB_TABLE_NAME = os.environ.get('SITES_TABLE_NAME')

STATUS_OK = 200
STATUS_CREATED = 201
STATUS_CONFLICT = 409
STATUS_ERROR = 500

# Helper class to convert a DynamoDB item to JSON.
class DecimalEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            if o % 1 > 0:
                return float(o)
            else:
                return int(o)
        return super(DecimalEncoder, self).default(o)


class DynamoDBClient(object):
    dynamodb = boto3.resource('dynamodb')
    dynamodb_client = boto3.client('dynamodb')
    table = dynamodb.Table(DYNAMODB_TABLE_NAME)
    
    @staticmethod
    def get_site(domain: str, just_count: bool=False):
        kwargs = {
            'TableName': DYNAMODB_TABLE_NAME,
            'KeyConditionExpression': 'PK=:PK AND begins_with(SK, :SK)',
            'ExpressionAttributeValues': {
                ':PK': {'S': 'SITE|{}'.format(domain)},
                ':SK': {'S': 'PAGE|'}
            }
        }
        if just_count:
            kwargs['Select'] = 'COUNT'
        response = DynamoDBClient.dynamodb_client.query(**kwargs)
        return response


def get_site(site: str) -> Tuple[dict, int]:
    data = DynamoDBClient.get_site(site)
    items = data['Items']
    paths = []
    deserializer = boto3.dynamodb.types.TypeDeserializer()
    for item in items:
        d = {k: deserializer.deserialize(value=v) for k, v in item.items()}
        paths.append(d)
    
    data = {
        'rootURL': site,
        'totalNumPages': len(paths),
        'pages': paths
    }
    return data, STATUS_OK


def lambda_handler(event, context) -> dict:
    
    site = event['pathParameters']['site']
    data, status_code = get_site(site)

    status = 'success' if 200 <= status_code < 300 else 'error'
    resp = {'status': status, 'data': data}
    resp_json = json.dumps(resp, cls=DecimalEncoder)

    return {'body': resp_json, 'statusCode': status_code, "headers": {"Content-Type": "application/json"}}
