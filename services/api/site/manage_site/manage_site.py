from typing import Tuple, Dict
import os
import boto3
from boto3.dynamodb.conditions import Key
import json
import decimal

DYNAMODB_TABLE_NAME = os.environ.get('SITES_TABLE_NAME')
SQS_CRAWLER_QUEUE_NAME = os.environ.get('CRAWLER_QUEUE_NAME')
SQS_SCREENSHOTS_QUEUE_NAME = os.environ.get('SCREENSHOTS_QUEUE_NAME')

STATUS_OK = 200
STATUS_CREATED = 201
STATUS_CONFLICT = 409
STATUS_ERROR = 500


def clean_url(url):
    return url.replace('https://', '').replace('http://', '').replace('www.', '')


class SQSClient(object):
    sqs = boto3.resource('sqs')
    crawler_queue_client = sqs.get_queue_by_name(QueueName=SQS_CRAWLER_QUEUE_NAME)
    screenshots_queue_client = sqs.get_queue_by_name(QueueName=SQS_SCREENSHOTS_QUEUE_NAME)
    @staticmethod
    def add_to_crawler_queue(url):
        print("Adding {} to crawl queue".format(url))
        response = SQSClient.crawler_queue_client.send_message(MessageBody=url, MessageAttributes={
            'URL': {
                'StringValue': url,
                'DataType': 'String'
            }
        })

        print("Added {} to crawl queue - queue id is {}".format(url, response.get('MessageId')))
        # print(response.get('MD5OfMessageBody'))
        return response

    @staticmethod
    def add_to_screenshots_queue(pk, sk, url, pageID, screenshotName, browserWidth, browserHeight, isFullPage, deviceName, filename):
        msg_attrs = {
            'PK': {
                'StringValue': pk,
                'DataType': 'String'
            },
            'SK': {
                'StringValue': sk,
                'DataType': 'String'
            },
            'url': {
                'StringValue': url,
                'DataType': 'String'
            },
            'pID': {
                'StringValue': pageID,
                'DataType': 'String'
            },
            'screenshotName': {
                'StringValue': screenshotName,
                'DataType': 'String'
            },
            'browserWidth': {
                'StringValue': str(browserWidth),
                'DataType': 'Number'
            },
            'browserHeight': {
                'StringValue': str(browserHeight),
                'DataType': 'Number'
            },
            'isFullPage': {
                'StringValue': "1" if isFullPage else "0",
                'DataType': 'Number'
            },
            'fileName': {
                'StringValue': filename,
                'DataType': 'String'
            }
            # 'deviceName': {
            #     'StringValue': deviceName if deviceName else "",
            #     'DataType': 'String'
            # },
        }
        if deviceName:
            msg_attrs['deviceName'] = {
                    'StringValue': deviceName,
                    'DataType': 'String'
            }
        print(msg_attrs.get('url'))
        response = SQSClient.screenshots_queue_client.send_message(MessageBody=url, MessageAttributes=msg_attrs)
        # print(response.get('MessageId'))
        return None


class DynamoDBClient(object):
    dynamodb = boto3.resource('dynamodb')
    dynamodb_client = boto3.client('dynamodb')
    table = dynamodb.Table(DYNAMODB_TABLE_NAME)
    
    @staticmethod
    def get_site(site: str):
        kwargs = {
            'TableName': DYNAMODB_TABLE_NAME,
            'KeyConditionExpression': 'PK=:PK AND begins_with(SK, :SK)',
            'ExpressionAttributeValues': {
                ':PK': {'S': 'SITE|{}'.format(site)},
                ':SK': {'S': 'PAGE|'}
            },
            'ProjectionExpression': "PK, SK, fullURL, pageID"
        }
        response = DynamoDBClient.dynamodb_client.query(**kwargs)
        return response

    @staticmethod
    def set_page_screenshots(pk, sk, screenshots):
        response = DynamoDBClient.table.update_item(
            Key={
                'PK': '{}'.format(pk),
                'SK': '{}'.format(sk)
            },
            UpdateExpression="set screenshots=:s",
            ExpressionAttributeValues={
                ':s': screenshots,
            },
            ReturnValues="UPDATED_NEW"
        )
        return response


class Screenshots(object):
    def __init__(self, site):
        self.site = site

    def retake_screenshots(self, site: str) -> Tuple[dict, int]:
        print("HERE", site)
        data = DynamoDBClient.get_site(site)
        items = data['Items']
        paths = []
        deserializer = boto3.dynamodb.types.TypeDeserializer()
        total_pages = 0
        success_counter = 0
        error_counter = 0
        urls = []
        for item in items:
            total_pages += 1
            d = {k: deserializer.deserialize(value=v) for k, v in item.items()}
            url = d.get('fullURL')
            print(url)
            urls.append(url)
            try:
                self.initiate_screenshots(d.get('PK'), d.get('SK'), url, d.get('pageID'))
                success_counter += 1
            except Exception as e:
                error_counter += 1
                print("{} was not added to screenshots queue to be re-taken because of an error - {}".format(url, str(e)))

        
        resp_data = {
            'site': site,
            'totalPages': total_pages,
            'screenshotsToBeRetaken': success_counter,
            'errors': error_counter,
            'urls': ', '.join(urls)
        }

        return resp_data, 200

    def initiate_screenshots(self, pk, sk, url, pageId) -> None:
        print("Initiating screenshots job for {}".format(url))
        screenshots_config = self.get_screenshot_config(url, pageId)
        self.initiate_db_record_for_screenshots(pk, sk, screenshots_config)
        for name, config in screenshots_config.items():
            SQSClient.add_to_screenshots_queue(pk, sk, url, pageId, name, config.get('w'), config.get('h'), config.get('is_full_page'), config.get('device_name'), config.get('filename'))
        print("Screenshots job has been initiated {}".format(url))

    def get_screenshot_config(self, url: str, pageId: str) -> Dict:
        url = clean_url(url)
        url = url.replace('/', '-')
        screenshot_config = {
            # 'mobile': {'w': 411, 'h': 731, 'is_full_page': False, 'device_name': 'iPhone X', 'filename': '{}_{}_{}.png'.format(str(self.id), url, "411-731")}, 
            # 'mobileFullPage': {'w': 10, 'h': 10, 'is_full_page': False, 'device_name': 'iPhone X', 'filename': '{}_{}_{}.png'.format(str(self.id), url, "mobileFullPage")}, 
            'desktop': {'w': 1920, 'h': 1080, 'is_full_page': False, 'device_name': '', 'filename': '{}_{}_{}.png'.format(str(pageId), url, "desktop")},
            # 'desktop2': {'w': 1920, 'h': 1080, 'is_full_page': False, 'device_name': '', 'filename': '{}_{}_{}.png'.format(str(self.id), url, "desktop2")},
            # 'desktopFullPage': {'w': 1920, 'h': 1080, 'is_full_page': True, 'device_name': '', 'filename': '{}_{}_{}.png'.format(str(self.id), url, "desktopFullPage")},
        }
        return screenshot_config

    def initiate_db_record_for_screenshots(self, pk: str, sk: str, screenshots_config: dict) -> None:
        blank_screenshots_map = {name: "" for name in screenshots_config.keys()}
        print("About to be initiated")
        DynamoDBClient.set_page_screenshots(pk, sk, blank_screenshots_map)


class TestResults(object):
    def __init__(self, site):
        self.site = site

    def run_tests(self):
        lambda_client = boto3.client('lambda')
        resp_data = {}
        status = STATUS_OK
        try:
            invoke_response = lambda_client.invoke(FunctionName="TestRunnerJob",
                                           InvocationType='RequestResponse'
                                           )
            print(invoke_response)
            resp_data['message'] = "TestRunnerJob triggered successfully"
        except Exception as e:
            status = STATUS_ERROR
            resp_data['message'] = "TestRunnerJob was not triggered because of an error - {}".format(str(e))
        
        return resp_data, status




def screenshots_handler(event, context) -> dict:

    site = event['pathParameters']['site']

    method = event['requestContext']['http']['method']
    data = {}
    status_code = STATUS_ERROR

    if method == 'POST':
        screenshots = Screenshots(site)
        data, status_code = screenshots.retake_screenshots(site)

    status = 'success' if 200 <= status_code < 300 else 'error'
    resp = {'status': status, 'data': data}
    resp_json = json.dumps(resp)

    return {'body': resp_json, 'statusCode': status_code, "headers": {"Content-Type": "application/json"}}


def test_results_handler(event, context):
    print("In Test Results Handler")
    print(event)
    site = event['pathParameters']['site']
    method = event['requestContext']['http']['method']
    data = {}
    status_code = STATUS_ERROR

    if method == 'POST':
        tests = TestResults(site)
        data, status_code = tests.run_tests()

    status = 'success' if 200 <= status_code < 300 else 'error'
    resp = {'status': status, 'data': data}
    resp_json = json.dumps(resp)

    return {'body': resp_json, 'statusCode': status_code, "headers": {"Content-Type": "application/json"}}
