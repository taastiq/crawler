const { time } = require("console")

function takeScreenshot(url, pageID, browserWidth, browserHeight, isFullPage, deviceName, fileName, db_partition_key, db_sort_key, screenshotName){
    console.log(`takeScreenshot function starting for ${url} - ${pageID}`)
    console.log("Importing webdriverio...")
    let { remote } = require("webdriverio")
    console.log("Importing wdio-image-comparison-service...")
    let WdioImageComparisonService = require('wdio-image-comparison-service').default
    console.log("Initializing wdio-image-comparison-service...")
    let wdioImageComparisonService = new WdioImageComparisonService({})
    let browser

    console.log("Importing fs")
    let fs = require('fs');
    console.log("Importing aws-sdk")
    let AWS = require('aws-sdk');

    console.log("All imports/function initialization have been completed")
    async function main() {
        console.log("Inside main")
        console.log("Preparing Selenium...")
        let capabilities = {
            browserName: "chrome",
            'goog:chromeOptions': {
                args: ['disable-extensions', '--window-size=2048,1536'],
                'excludeSwitches': ['enable-automation']
            },
            // 'moon:options': {
            //     enableVNC: true,
            //     enableVideo: false,
            //     screenResolution: '2048x1536x24',
            //     enableLog: false
            // },
            'selenoid:options': {
                enableVNC: true,
                enableVideo: false,
                screenResolution: '2048x1536x24',
                enableLog: false
            }
        }
        if(deviceName){
            capabilities['goog:chromeOptions'].mobileEmulation = {
                deviceName: deviceName
            }
        }
        browser = await remote({
            hostname: '167.172.144.73', //'moon.3.139.34.189.xip.io', //'167.172.144.73'
            port: 4444, //80, //4444
            path: '/wd/hub',
            logLevel: "silent",
            capabilities: capabilities
        })
        console.log("Selenium initiated")
        console.log("Starting wdioImageComparisonService...")
        global.browser = browser;
        wdioImageComparisonService.defaultOptions.autoSaveBaseline = true
        wdioImageComparisonService.defaultOptions.disableCSSAnimation = true
        wdioImageComparisonService.defaultOptions.formatImageName = '{tag}'
        wdioImageComparisonService.defaultOptions.screenshotPath = '/tmp'
        browser.defaultOptions = wdioImageComparisonService.defaultOptions
        browser.folders = wdioImageComparisonService.folders
        wdioImageComparisonService.before(browser.capabilities)

        let methodOptions = {
            actualFolder: '/tmp',
            baselineFolder: '/tmp',
            diffFolder: '/tmp',
        };
        if(!deviceName) {
            await browser.setWindowSize(browserWidth, browserHeight)
        }
        console.log("Loading URL...")
        await browser.url(url)
        let timeout = 2000;
        console.log(`Waiting preset timeout of ${timeout} for page to load... `);
        await browser.pause(timeout)
        console.log("Starting to take a screenshot")
        let fileNameNoExt = fileName.replace('.png', '')
        let screenshotOptions = methodOptions
        if(isFullPage) {
            let header = await browser.$('header')
            if(header){
                screenshotOptions.hideAfterFirstScroll = [
                  header
                ]
              }
            await browser.saveFullPageScreen(fileNameNoExt, screenshotOptions)
        } else {
            await browser.saveScreen(fileNameNoExt, screenshotOptions)
        }

        await browser.deleteSession();
        console.log("Screenshot successfully taken")
        
        console.log("Preparing S3 upload...")
        let s3 = new AWS.S3()
        try{
            let fileContent = fs.readFileSync('/tmp/'+ fileName);
            // Setting up S3 upload parameters
            let params = {
                Bucket: process.env.SCREENSHOTS_BUCKET_NAME,
                Key: fileName,
                Body: fileContent
            };
            let res = await s3.upload(params).promise()
        } catch (error) {
            console.log(error);
            return;
        } 
        console.log(`Screenshot ${fileName} uploaded to S3`)
        
        console.log("Preparing DB updated...")
        let dbClient = new AWS.DynamoDB.DocumentClient();
        let table = process.env.SITES_TABLE_NAME
        let params = {
            TableName:table,
            Key:{
                "PK": db_partition_key,
                "SK": db_sort_key
            },
            UpdateExpression: "set screenshots.#screenshotName = :filename",
            ExpressionAttributeNames: {
                "#screenshotName": screenshotName
            },
            ExpressionAttributeValues:{
                ":filename": fileName
            },
            ReturnValues:"UPDATED_NEW"
        };
        console.log("Setting screenshot name in the DB...");
        try{
            await dbClient.update(params).promise();
        }
        catch (error){
            console.error("Unable to update item. Error JSON:", JSON.stringify(error, null, 2));
            return;
        }
        console.log("DB successfully updated");

    }
    return main().catch(async e => {
        console.error(e)
    })
}

// exports.handler =  async function(event, context) {
//     console.log("screenshots.js lambda function is starting...")
//     console.log("Reading event body and message attributes...")
//     // let msgBody = event.MessageBody;
//     let promises = []

//     await Promise.all(
//         event.Records.map(async (record) => {
//             let msg = record.messageAttributes;
//             console.log(msg);
//             // console.log(msg.PK);
//             // console.log(msg.deviceName);
//             // console.log(msg.browserHeight);
//             let db_partition_key = msg.PK.stringValue;
//             let db_sort_key = msg.SK.stringValue;
//             let url = msg.url.stringValue;
//             let pID = msg.pID.stringValue;
//             let screenshotName = msg.screenshotName.stringValue;
//             console.log(screenshotName);
//             let browserWidth = parseInt(msg.browserWidth.stringValue);
//             let browserHeight = parseInt(msg.browserHeight.stringValue);
//             let isFullPage = parseInt(msg.isFullPage.stringValue);
//             let deviceName = null;
//             if (msg.deviceName != undefined){
//                 deviceName = msg.deviceName.stringValue;
//             }
//             let filename = msg.fileName.stringValue;
//             console.log("The following information was passed through - ", url, pID, browserWidth, browserHeight, isFullPage, deviceName, filename, db_partition_key, db_sort_key, screenshotName)
        
//             await takeScreenshot(url, pID, browserWidth, browserHeight, isFullPage, deviceName, filename, db_partition_key, db_sort_key, screenshotName)
//         })
//     )

//     return {
//         statusCode: 200,
//         body: JSON.stringify("Screenshot function finised")
//     }
// }

exports.handler =  async function(event, context) {
    console.log("screenshots.js lambda function is starting...")
    console.log("Reading event body and message attributes...")
    // let msgBody = event.MessageBody;
    let promises = []

    for (const record of event.Records) {     
        let msg = record.messageAttributes;
        console.log(msg);
        // console.log(msg.PK);
        // console.log(msg.deviceName);
        // console.log(msg.browserHeight);
        let db_partition_key = msg.PK.stringValue;
        let db_sort_key = msg.SK.stringValue;
        let url = msg.url.stringValue;
        let pID = msg.pID.stringValue;
        let screenshotName = msg.screenshotName.stringValue;
        console.log(screenshotName);
        let browserWidth = parseInt(msg.browserWidth.stringValue);
        let browserHeight = parseInt(msg.browserHeight.stringValue);
        let isFullPage = parseInt(msg.isFullPage.stringValue);
        let deviceName = null;
        if (msg.deviceName != undefined){
            deviceName = msg.deviceName.stringValue;
        }
        let filename = msg.fileName.stringValue;
        console.log("The following information was passed through - ", url, pID, browserWidth, browserHeight, isFullPage, deviceName, filename, db_partition_key, db_sort_key, screenshotName)
    
        await takeScreenshot(url, pID, browserWidth, browserHeight, isFullPage, deviceName, filename, db_partition_key, db_sort_key, screenshotName)
    }

    return {
        statusCode: 200,
        body: JSON.stringify("Screenshot function finised")
    }
}


    // event.Records.forEach(async record => {
    //     let msg = record.messageAttributes;
    //     console.log(msg);
    //     // console.log(msg.PK);
    //     // console.log(msg.deviceName);
    //     // console.log(msg.browserHeight);
    //     let db_partition_key = msg.PK.stringValue;
    //     let db_sort_key = msg.SK.stringValue;
    //     let url = msg.url.stringValue;
    //     let pID = msg.pID.stringValue;
    //     let screenshotName = msg.screenshotName.stringValue;
    //     console.log(screenshotName);
    //     let browserWidth = parseInt(msg.browserWidth.stringValue);
    //     let browserHeight = parseInt(msg.browserHeight.stringValue);
    //     let isFullPage = parseInt(msg.isFullPage.stringValue);
    //     let deviceName = null;
    //     if (msg.deviceName != undefined){
    //         deviceName = msg.deviceName.stringValue;
    //     }
    //     let filename = msg.fileName.stringValue;
    //     console.log("The following information was passed through - ", url, pID, browserWidth, browserHeight, isFullPage, deviceName, filename, db_partition_key, db_sort_key, screenshotName)
    
    //     promises.push(takeScreenshot(url, pID, browserWidth, browserHeight, isFullPage, deviceName, filename, db_partition_key, db_sort_key, screenshotName))
    // });
    // return Promise.all(promises);
    
//   }