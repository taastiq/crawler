import os
import uuid
from urllib.parse import urlparse, urljoin
import time
import json
import boto3
from boto3.dynamodb.conditions import Key
from botocore.exceptions import ClientError
from selenium import webdriver
from selenium.webdriver.remote.remote_connection import RemoteConnection
import validators
from bs4 import BeautifulSoup

# SELENIUM_REMOTE_CONNECTION_URL = 'http://moon.3.139.34.189.xip.io/wd/hub'
SELENIUM_REMOTE_CONNECTION_URL = 'http://167.172.144.73:4444/wd/hub'

DYNAMODB_TABLE_NAME = os.environ.get('SITES_TABLE_NAME')
SQS_CRAWLER_QUEUE_NAME = os.environ.get('CRAWLER_QUEUE_NAME')
SQS_SCREENSHOTS_QUEUE_NAME = os.environ.get('SCREENSHOTS_QUEUE_NAME')


def loginfo(msg):
    print("INFO: {}".format(msg))


def logerror(msg):
    print("ERROR: {}".format(msg))


def is_valid_url(url):
    return validators.url(url)


def clean_url(url):
    return url.replace('https://', '').replace('http://', '').replace('www.', '')

class SQSClient(object):
    sqs = boto3.resource('sqs')
    crawler_queue_client = sqs.get_queue_by_name(QueueName=SQS_CRAWLER_QUEUE_NAME)
    screenshots_queue_client = sqs.get_queue_by_name(QueueName=SQS_SCREENSHOTS_QUEUE_NAME)
    @staticmethod
    def add_to_crawler_queue(url):
        print("Adding {} to crawl queue".format(url))
        response = SQSClient.crawler_queue_client.send_message(MessageBody=url, MessageAttributes={
            'URL': {
                'StringValue': url,
                'DataType': 'String'
            }
        })

        print("Added {} to crawl queue - queue id is {}".format(url, response.get('MessageId')))
        # print(response.get('MD5OfMessageBody'))
        return response

    @staticmethod
    def add_to_screenshots_queue(pk, sk, url, pageID, screenshotName, browserWidth, browserHeight, isFullPage, deviceName, filename):
        msg_attrs = {
            'PK': {
                'StringValue': pk,
                'DataType': 'String'
            },
            'SK': {
                'StringValue': sk,
                'DataType': 'String'
            },
            'url': {
                'StringValue': url,
                'DataType': 'String'
            },
            'pID': {
                'StringValue': pageID,
                'DataType': 'String'
            },
            'screenshotName': {
                'StringValue': screenshotName,
                'DataType': 'String'
            },
            'browserWidth': {
                'StringValue': str(browserWidth),
                'DataType': 'Number'
            },
            'browserHeight': {
                'StringValue': str(browserHeight),
                'DataType': 'Number'
            },
            'isFullPage': {
                'StringValue': "1" if isFullPage else "0",
                'DataType': 'Number'
            },
            'fileName': {
                'StringValue': filename,
                'DataType': 'String'
            }
            # 'deviceName': {
            #     'StringValue': deviceName if deviceName else "",
            #     'DataType': 'String'
            # },
        }
        if deviceName:
            msg_attrs['deviceName'] = {
                    'StringValue': deviceName,
                    'DataType': 'String'
            }
        response = SQSClient.screenshots_queue_client.send_message(MessageBody=url, MessageAttributes=msg_attrs)

        print(response.get('MessageId'))
        print(response.get('MD5OfMessageBody'))
        return response


class DynamoDBClient(object):
    dynamodb = boto3.resource('dynamodb')
    dynamodb_client = boto3.client('dynamodb')
    table = dynamodb.Table(DYNAMODB_TABLE_NAME)
    
    @staticmethod
    def get(domain, path):
        response = DynamoDBClient.dynamodb_client.query(
            TableName = DYNAMODB_TABLE_NAME,
            KeyConditionExpression='PK=:PK AND SK=:SK',
            ExpressionAttributeValues = {
                ':PK': {'S': 'SITE|{}'.format(domain)},
                ':SK': {'S': 'PAGE|{}'.format(path)}
            }
        )
        if response['Count'] > 0:
            return response['Items'][0]
        else:
            return {}

    @staticmethod
    def get_attr_value(obj, attr):
        return list(obj.get(attr).values())[0]

    @staticmethod
    def add_site_to_db(site):
        metadata_item = {
            'PK': {'S': 'SITE|{}'.format(site)},
            'SK': {'S': 'METADATA|{}'.format(site)},
            'IsCrawled': {'BOOL': False}
        }
        print(metadata_item)
        resp = DynamoDBClient.dynamodb_client.put_item(
            TableName = DYNAMODB_TABLE_NAME,
            Item = metadata_item
        )
        page_item = {
            'PK': {'S': 'SITE|{}'.format(site)},
            'SK': {'S': 'PAGE|/'},
            'IsCrawled': {'BOOL': False}
        }
        resp = DynamoDBClient.dynamodb_client.put_item(
            TableName = DYNAMODB_TABLE_NAME,
            Item = page_item
        )
        return page_item

    @staticmethod
    def add_site_page_to_db(hostname, path, full_url, id, parent_id=None):
        page_item = {
            'PK': {'S': 'SITE|{}'.format(hostname)},
            'SK': {'S': 'PAGE|{}'.format(path)},
            'hostname': {'S': hostname},
            'isCrawled': {'BOOL': False},
            'isCrawlInProgress': {'BOOL': False},
            'path': {'S': path},
            'fullURL': {'S': full_url},
            'id': {'S': id},
            'pageID': {'S': id},
            'screenshots': {'M': {}},
            'latestTestResults': {'M': {}},
        }
        if parent_id:
            page_item['parentID'] = {'S': parent_id}
        
        resp = DynamoDBClient.dynamodb_client.put_item(
            TableName = DYNAMODB_TABLE_NAME,
            Item = page_item
        )
        return page_item

    @staticmethod
    def check_if_site_exists_db(site):
        response = DynamoDBClient.table.query(
            KeyConditionExpression=Key('PK').eq('SITE|' + site)
            )
        items = response['Items']
        return items

    @staticmethod
    def set_page_as_crawled(hostname, path):
        response = DynamoDBClient.table.update_item(
            Key={
                'PK': 'SITE|{}'.format(hostname),
                'SK': 'PAGE|{}'.format(path)
            },
            UpdateExpression="set isCrawled=:c",
            ExpressionAttributeValues={
                ':c': True,
            },
            ReturnValues="UPDATED_NEW"
        )
        return response

    @staticmethod
    def set_page_crawl_state(hostname, path, state):
        response = DynamoDBClient.table.update_item(
            Key={
                'PK': 'SITE|{}'.format(hostname),
                'SK': 'PAGE|{}'.format(path)
            },
            UpdateExpression="set isCrawlInProgress=:c",
            ExpressionAttributeValues={
                ':c': state,
            },
            ReturnValues="UPDATED_NEW"
        )
        return response

    @staticmethod
    def set_page_screenshots(hostname, path, screenshots):
        response = DynamoDBClient.table.update_item(
            Key={
                'PK': 'SITE|{}'.format(hostname),
                'SK': 'PAGE|{}'.format(path)
            },
            UpdateExpression="set screenshots=:s",
            ExpressionAttributeValues={
                ':s': screenshots,
            },
            ReturnValues="UPDATED_NEW"
        )
        return response

    @staticmethod
    def set_crawl_time(hostname, path, time):
        response = DynamoDBClient.table.update_item(
            Key={
                'PK': 'SITE|{}'.format(hostname),
                'SK': 'PAGE|{}'.format(path)
            },
            UpdateExpression="set crawlTime=:t",
            ExpressionAttributeValues={
                ':t': time,
            },
            ReturnValues="UPDATED_NEW"
        )
        return response


class Page(object):
    def __init__(self, url, parent_id=None):
        self.url = url
        self.parent_id = parent_id
        url_obj = urlparse(url)
        self.scheme = url_obj.scheme
        self.hostname = clean_url(url_obj.hostname)
        self.path = url_obj.path if url_obj.path else '/'
        self.existed_in_db = None
        self.page_db_obj = self.get_or_create()
        self.external_urls = set()
        self.id = DynamoDBClient.get_attr_value(self.page_db_obj, 'id')
        self.pk = DynamoDBClient.get_attr_value(self.page_db_obj, 'PK')
        self.sk = DynamoDBClient.get_attr_value(self.page_db_obj, 'SK')
        self.is_crawled = DynamoDBClient.get_attr_value(self.page_db_obj, 'isCrawled')
        self.is_crawl_in_progress = DynamoDBClient.get_attr_value(self.page_db_obj, 'isCrawlInProgress')
        self._source = ""
        self.source_soup = None
        self.children_urls = set()

    def get_or_create(self):
        print("Getting Page object for {}".format(self.url))
        page_db_obj = DynamoDBClient.get(self.hostname, self.path)
        if not page_db_obj:
            print("Page {} does not exist in DB. Adding new entry...".format(self.url))
            page_db_obj = self.save_page_to_db()
            self.existed_in_db = False
            print("Page object for {} added to DB".format(self.url))
        else:
            # print("Page {} already exists in DB. Skipping adding to DB".format(self.url))
            self.existed_in_db = True
        return page_db_obj

    def crawl(self):
        print('Crawl of {} starting'.format(self.url))
        # self.set_crawl_state(True)

        urls = set()
        links = self.source_soup.find_all('a')
        for link in links:
            href = link.get("href")
            url = href
            href_hostname = urlparse(href).hostname
            if not href_hostname:
                url = urljoin(self.url, href)
            if url and url not in urls and is_valid_url(url):
                if not clean_url(urlparse(url).hostname) == self.hostname:
                    self.external_urls.add(url)
                else:
                    urls.add(url)
        print("Following URLs found on the page {} - {}".format(self.url, ', '.join(list(urls))))
        
        self.children_urls = urls

        DynamoDBClient.set_page_as_crawled(self.hostname, self.path)
        self.set_crawl_state(False)
        # DynamoDBClient.set_page_crawl_state(self.hostname, self.path, False)

        print("Crawl of {} finished".format(self.url))
        return urls

    def set_crawl_state(self, is_in_progress):
        DynamoDBClient.set_page_crawl_state(self.hostname, self.path, is_in_progress)

    def initiate_page_children_crawl(self):
        print("Initiating crawl of {} child pages".format(self.url))
        for url in self.children_urls:
            child_page_obj = Page(url, parent_id=self.id)
            if not child_page_obj.existed_in_db:
                print("{} hasn't been crawled before. Adding it to the queue".format(child_page_obj.url))
                child_page_obj.send_to_crawl_queue()
        print("Crawl of child pages has been initiated")

    def initiate_screenshots(self):
        print("Initiating screenshots job for {}".format(self.url))
        screenshots_config = self.get_screenshot_config()
        self.initiate_db_record_for_screenshots(screenshots_config)
        for name, config in screenshots_config.items():
            SQSClient.add_to_screenshots_queue(self.pk, self.sk, self.url, self.id, name, config.get('w'), config.get('h'), config.get('is_full_page'), config.get('device_name'), config.get('filename'))
        print("Screenshots job has been initiated {}".format(self.url))

    def get_screenshot_config(self):
        url = clean_url(self.url)
        url = url.replace('/', '-')
        screenshot_config = {
            # 'mobile': {'w': 411, 'h': 731, 'is_full_page': False, 'device_name': 'iPhone X', 'filename': '{}_{}_{}.png'.format(str(self.id), url, "411-731")}, 
            # 'mobileFullPage': {'w': 10, 'h': 10, 'is_full_page': False, 'device_name': 'iPhone X', 'filename': '{}_{}_{}.png'.format(str(self.id), url, "mobileFullPage")}, 
            'desktop': {'w': 1920, 'h': 1080, 'is_full_page': False, 'device_name': '', 'filename': '{}_{}_{}.png'.format(str(self.id), url, "desktop")},
            # 'desktop2': {'w': 1920, 'h': 1080, 'is_full_page': False, 'device_name': '', 'filename': '{}_{}_{}.png'.format(str(self.id), url, "desktop2")},
            # 'desktopFullPage': {'w': 1920, 'h': 1080, 'is_full_page': True, 'device_name': '', 'filename': '{}_{}_{}.png'.format(str(self.id), url, "desktopFullPage")},
        }
        return screenshot_config

    def initiate_db_record_for_screenshots(self, screenshots_config):
        blank_screenshots_map = {name: "" for name in screenshots_config.keys()}
        DynamoDBClient.set_page_screenshots(self.hostname, self.path, blank_screenshots_map)

    def save_page_to_db(self):
        page_id = str(uuid.uuid4())
        page_db_obj = DynamoDBClient.add_site_page_to_db(self.hostname, self.path, self.url, page_id, self.parent_id)
        return page_db_obj

    def send_to_crawl_queue(self):
        # return
        SQSClient.add_to_crawler_queue(self.url)

    @property
    def source(self):
        return self._source

    @source.setter
    def source(self, s):
        self._source = s
        self.source_soup = BeautifulSoup(s, 'html.parser')


class SeleniumDriver(object):
    def __init__(self, remote_connection_url):
        self.remote_connection_url = remote_connection_url
        self.driver = self.get_driver()

    
    def get_driver(self):
        chrome_options = webdriver.ChromeOptions()
        # resolution_config = {
        #     'mobile': '411x731',
        #     'desktop': '1368x768'
        # }
        # resolution = resolution_config.get('desktop')
        
        # chrome_options.add_argument('--headless')
        # chrome_options.add_argument('--no-sandbox')
        # chrome_options.add_argument('--single-process')
        # chrome_options.add_argument('--disable-dev-shm-usage')
        # chrome_options.add_argument('--start-maximized')
        # chrome_options.add_argument('--window-size={}'.format(resolution))

        remote_connection = RemoteConnection(self.remote_connection_url, resolve_ip=False)
        driver = webdriver.Remote(remote_connection, chrome_options.to_capabilities()) 

        # driver = webdriver.Remote(RemoteConnection(self.remote_connection_url, resolve_ip=False), chrome_options.to_capabilities())
        # driver = webdriver.Remote("http://167.172.144.73:4444/wd/hub", chrome_options.to_capabilities())
        return driver

    def close(self):
        self.driver.quit()

    def get_page_info(self, url):
        self.driver.get(url)
        self.driver.implicitly_wait(1)
        time.sleep(1)
        # title = self.driver.title
        page_source = self.driver.page_source
        return {'source': page_source}
    
def lambda_handler(event, context):
    # print("Lambda function ARN:", context.invoked_function_arn)
    print("CloudWatch log stream name:", context.log_stream_name)
    print("CloudWatch log group name:",  context.log_group_name)
    print("Lambda Request ID:", context.aws_request_id)
    # print("Lambda function memory limits in MB:", context.memory_limit_in_mb)
    print(event)
    # driver = None
    records = event['Records']
    for record in records:
        print(record)
        url=record["body"]
        is_force = record["messageAttributes"].get("isForce", {}).get("stringValue", 0)
        print("Starting for {}".format(url))
        print("Force is {}".format('enabled' if is_force else 'disabled'))
        page_obj = Page(url)
        if not is_force:
            if page_obj.is_crawled:
                print('Page {} already crawled. Aborting...'.format(page_obj.url))
                return
            if page_obj.is_crawl_in_progress:
                print('Page {} already being crawled. Aborting...'.format(page_obj.url))
                return
        page_obj.set_crawl_state(True)
        print("Initiating Selenium Driver")
        # if not driver:
        driver = SeleniumDriver(SELENIUM_REMOTE_CONNECTION_URL)
        print("Getting page souce")
        page_info = driver.get_page_info(url)
        driver.close()
        print("Got page source and closed the driver")
        page_obj.source = page_info.get('source')
    
        page_obj.crawl()
        # page_obj.initiate_screenshots()
        page_obj.initiate_page_children_crawl()
        print("Done processing record")
    print("Finish processing all records in an event")
