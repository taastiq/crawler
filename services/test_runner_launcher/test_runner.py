from typing import Tuple, Dict
import json
import requests

TEKTON_URL = 'http://tekton-wh.3.139.34.189.xip.io/'

def log(msg) -> None:
    print(msg)


def launch_test() -> Tuple[dict, int]:
    try:
        log("Starting tests...")
        r = requests.get(TEKTON_URL)
        if r.status_code in (200, 201):
            msg = 'Tests started successfully'
            log(msg)
            log('Response from test server - {}'.format(r.text))
            return {'message': msg}, 200
        else:
            msg = {'message': 'Error occured when starting tests - {} - {}'.format(r.status_code, r.text)}
            log(msg)
            return msg, 500    
    except Exception as e:
        msg = {'message': 'Error occured when starting tests - {}'.format(str(e))}
        log(msg)
        return msg, 500


def lambda_handler(event, context) -> dict:
    log("In Test Runner")
    data, status_code = launch_test()
    
    status = 'success' if 200 <= status_code < 300 else 'error'
    resp = {'status': status, 'data': data}
    resp_json = json.dumps(resp)
    log(resp_json)

    return {'body': resp_json, 'statusCode': status_code, "headers": {"Content-Type": "application/json"}}
