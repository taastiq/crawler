#!/bin/bash

env="dev"

stackName="awarebee-dev"

echo "Validatating template...  (won't find all problems)"
sam validate

echo "Building $stackName stack..."
sam build

echo "Deploying to $env..."

sam deploy --guided --region "us-east-2" --stack-name $stackName --parameter-overrides "ParameterKey=DeploymentEnvironment,ParameterValue=dev"
