
$allowedEnvs = "local", "dev", "prod"

$env=$args[0]

if ($env -notin $allowedEnvs){
    Write-Host "$env is not allowed value"
    return 1
}

$stackName = ""
if ($env -eq "dev"){
    $stackName = "awarebee-dev"
}
if ($env -eq "prod"){
    $stackName = "awarebee-prod"
}

Write-Host "Validatating template...  (won't find all problems)"
sam validate

Write-Host "Building $stackName stack..."
sam build

Write-Host "Deploying to $env..."

sam deploy --guided --region "us-east-2" --stack-name $stackName --parameter-overrides "ParameterKey=DeploymentEnvironment,ParameterValue=dev"
